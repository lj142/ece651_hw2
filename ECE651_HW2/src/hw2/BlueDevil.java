package hw2;
import java.util.*;
public class BlueDevil extends Person {
	private String major;
	private String studentID;
	private static List<BlueDevil> list = new ArrayList<BlueDevil>();
	private static Set<BlueDevil> set = new HashSet<BlueDevil>();
	public BlueDevil (String firstName, String lastName, String gender, 
			List<Map.Entry<String, Integer>> workExp, 
			List<String> hobbies, String nationality, List<Map.Entry<String, String>> education, 
			String job, String major, String studentID) {
		super(firstName, lastName, gender, workExp, hobbies, nationality, education, job);
		this.major = major;
		this.studentID = studentID;
	}
	public String getMajor() {
		return major;
	}
	public String getStudentID() {
		return studentID;
	}
	@Override
	public String toStringHelper() {
		StringBuilder sb = new StringBuilder();
		sb.append("When not in class, " + super.getFirstName() + " enjoys ");
		return sb.toString();
	}
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof BlueDevil)) {
			return false;
		}
		BlueDevil p = (BlueDevil) o;
		return studentID.equals(p.studentID);  
	}
	@Override
	public int hashCode() {
		int result = 31 * studentID.hashCode();
		return result;
	}
	// interaction with users to find a BlueDevil if he exists in the list
	public static void findPerson() {
		System.out.println("Sir/Madam, you want to search the person by...");
		System.out.println("(first name, last name, full name, gender, nationality, studentID)");
		Scanner scanner = new Scanner(System.in);
		String choice = "";
		while (!choice.equals("first name") && !choice.equals("last name") 
				&& !choice.equals("full name") && !choice.equals("gender")
				&& !choice.equals("nationality") && !choice.equals("studentID")) {
			choice = scanner.nextLine();
			if (choice.equals("first name")) {
				System.out.println("Please input the first name: ");
				String first = scanner.next();
				searchByFirstName(first);
			} else if (choice.equals("last name")) {
				System.out.println("Please input the last name: ");
				String last = scanner.next();
				searchByLastName(last);
			} else if (choice.equals("full name")) {
				System.out.println("Please input the first name: ");
				String first = scanner.next();
				System.out.println("Please input the last name: ");
				String last = scanner.next();
				whoIs(first, last);
			} else if (choice.equals("gender")) {
				System.out.println("Please input the gender:     (male/female)");
				String gender = scanner.next();
				searchByGender(gender);
			} else if (choice.equals("nationality")) {
				System.out.println("Please input the nationality: ");
				String nationality = scanner.next();
				searchByNationality(nationality);
			} else if (choice.equals("studentID")) {
				System.out.println("Please input the studentID: ");
				String studentID = scanner.next();
			} else {
				System.out.println("Please input the valid search condition");
				System.out.println("Please choose from first name, last name, full name, gender, nationality, studentID");
			}
		}
	}
	// interaction with users to remove a BlueDevil if he doesn't exist in the list
	public static void addPerson() {
		System.out.println("Please input the information of that person in following format: ");
		System.out.println("fist name, last name, gender (male/female), company (no working experience please type in \"no\", working years(should be a number), hobbies(separated by \"&\")");
		System.out.println("nationality, degree (undergraduate/graduate), unviersity, current job, department, duke ID");
		System.out.println("Please input information in two lines, tokens must be separated by commas");
		Scanner scanner = new Scanner(System.in);
		String line1 = scanner.nextLine();
		String line2 = scanner.nextLine();
		String[] arr1 = line1.split(",");
		String[] arr2 = line2.split(",");
		
		List<String> hobbies = new ArrayList<String>();
		String[] hstr = arr1[5].split("&");
		for (int i = 0; i < hstr.length; i++) {
			hobbies.add(hstr[i].trim());
		}
		List<Map.Entry<String, String>> education = new ArrayList<Map.Entry<String, String>>();
		education.add(new AbstractMap.SimpleEntry<String, String>(arr2[1].trim().toLowerCase(), arr2[2].trim()));
		List<Map.Entry<String, Integer>> preExp = new ArrayList<Map.Entry<String, Integer>>();
		if (!arr1[3].trim().equals("no")) {
			preExp.add(new AbstractMap.SimpleEntry<String, Integer>(arr1[3].trim(), Integer.parseInt(arr1[4].trim())));
		}
		BlueDevil person = new BlueDevil(arr1[0].trim(), arr1[1].trim(), arr1[2].trim(), preExp, 
			hobbies, arr2[0].trim(), education, arr2[3].trim(), arr2[4].trim(), arr2[5].trim());
		if (add(person)) {
			System.out.println("Add that person successfully!");
		} else {
			System.out.println("Input information is invalid, add failure...");
		}
	}
	// interaction with users to remove a BlueDevil if he exists in the list
	public static void removePerson() {
		System.out.println("Please input the first name of that person");
		Scanner scanner = new Scanner(System.in);
		String first = scanner.nextLine();
		System.out.println("Please input the last name of that person");
		String last = scanner.nextLine();
		boolean removed = false;
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getFirstName().equals(first) 
					&& list.get(i).getLastName().equals(last)) {
				remove(list.get(i));
				removed = true;
				System.out.println("Remove that person successfully");
				break;
			}
		}
		if (!removed) {
			System.out.println("There is no such a person, remove failure...");
		}
	}
	// add a BlueDevil to list
	public static boolean add(BlueDevil p) {
		if (set.contains(p)) {
			return false;
		}
		set.add(p);
		list.add(p);
		return true;
	}
	// remove a BlueDevil to list
	public static boolean remove(BlueDevil p) {
		if (!set.contains(p)) {
			return false;
		}
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).equals(p)) {
				list.remove(i);
			}
		}
		set.remove(p);
		return true;
	}
	// find a person by full name
	public static void whoIs(String first, String last) {
		if (first.length() == 0 || last.length() == 0) {
			return;
		}
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getFirstName().equals(first) 
					&& list.get(i).getLastName().equals(last)) {
				System.out.println(list.get(i).toString());
			}
		}
	}
	// find a person by first name
	public static void searchByFirstName(String first) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getFirstName().equals(first)) {
				System.out.println(list.get(i).toString());
			}
		}
	}
	// find a person by last name
	public static void searchByLastName(String last) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getLastName().equals(last)) {
				System.out.println(list.get(i).toString());
			}
		}
	}
	// find a person by gender
	public static void searchByGender(String gender) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getGender().equals(gender)) {
				System.out.println(list.get(i).toString());
			}
		}
	}
	// find a person by nationality
	public static void searchByNationality(String nationality) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getNationality().equals(nationality)) {
				System.out.println(list.get(i).toString());
			}
		}
	}
	// find a person by studentID
	public static void searchByStudentID(String id) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getStudentID().equals(id)) {
				System.out.println(list.get(i).toString());
			}
		}
	}
}
