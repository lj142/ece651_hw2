package hw2;
import java.util.*;
/**
 * @author jin0abc0
 *
 */
public class Person {
	private String firstName;
	private String lastName;
	private String gender;
	private List<Map.Entry<String, Integer>> workExp;
	private List<String> hobbies;
	private String nationality;
	private List<Map.Entry<String, String>> education;
	private Map<Person, Integer> favorability;
	private String job;

	public Person(String firstName, String lastName, String gender, List<Map.Entry<String, Integer>> workExp, 
			List<String> hobbies, String nationality, List<Map.Entry<String, String>> education, String job) {
		assert(gender.toLowerCase().equals("male") || gender.toLowerCase().equals("female"));
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender.toLowerCase();
		this.workExp = workExp;
		this.hobbies = hobbies;
		this.nationality = nationality;
		this.education = education;
		this.job = job;
		favorability = new HashMap<Person, Integer>();
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getGender() {
		return gender;
	}
	public List<Map.Entry<String, Integer>> getWorkExp() {
		return workExp;
	}
	public List<String> getHobbies() {
		return hobbies;
	}
	public String getNationality() {
		return nationality;
	}
	public List<Map.Entry<String, String>> getEducation() {
		return education;
	}
	public Map<Person, Integer> getFavorability() {
		return favorability;
	}
	public String getJob() {
		return job;
	}
	public String toStringHelper() {
		StringBuilder sb = new StringBuilder();
		sb.append("When in leisure time, " + firstName + " enjoys ");
		return sb.toString();
	}
	@Override
	public String toString() {
		String pron = gender.equals("male") ? "He" : "She"; 
		String poss = gender.equals("male") ? "his" : "her";
		StringBuilder sb = new StringBuilder();
		sb.append(firstName + " " + lastName);
		sb.append(" is from " + nationality + " and is a " + job + ". ");
		if (workExp.size() == 0) {
			sb.append(pron + " does not have prior work experience. ");
		} else {
			for (int i = 0; i < workExp.size(); i++) {
				sb.append(pron + " has been working in " + workExp.get(i).getKey() + 
						" for " + workExp.get(i).getValue() + " years. ");
			}
		}
		for (int i = 0; i < education.size(); i++) {
			sb.append(pron + " received " + poss + " " + education.get(i).getKey() + 
					" from " + education.get(i).getValue() + ". ");
		}
		if (hobbies.size() > 0) {
			sb.append(toStringHelper());
			if (hobbies.size() > 1) {
				for (int i = 0; i < hobbies.size() - 1; i++) {
					sb.append(hobbies.get(i) + " ");
				}
				sb.append("and " + hobbies.get(hobbies.size() - 1) + ".");
			} else {
				sb.append(hobbies.get(0) + ".");
			}
		}
		return sb.toString();
	}
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof Person)) {
			return false;
		}
		Person p = (Person) o;
		return firstName.equals(p.getFirstName()) && lastName.equals(p.getLastName()) 
				&& gender.equals(p.gender) && nationality.equals(p.nationality);  
	}
	@Override
	public int hashCode() {
		int result = 31 * firstName.hashCode() + 31 * 31 * lastName.hashCode() 
			+ 31 * 31 * 31 * gender.hashCode() + 31 * 31 * 31 * 31 * nationality.hashCode();
		return result;
	}	
}
