package hw2;

import java.util.*;
public class Scenario {
	public static void main(String[] args) {
		// yuanyuan
		List<String> yuanyuanHobbies = new ArrayList<String>();
		yuanyuanHobbies.add("baseball");
		yuanyuanHobbies.add("fencing");
		List<Map.Entry<String, String>> yuanyuanEducation = new ArrayList<Map.Entry<String, String>>();
		yuanyuanEducation.add(new AbstractMap.SimpleEntry<String, String>("undergraduate", "ECUST"));
		BlueDevil yuanyuan = new BlueDevil("Yuanyuan", "Yu", "female", new ArrayList<Map.Entry<String, Integer>>(), 
			yuanyuanHobbies, "China", yuanyuanEducation, "master ECE candidate", "MEng", "rt113");
		BlueDevil.add(yuanyuan);
		
		// lei
		List<String> leiHobbies = new ArrayList<String>();
		leiHobbies.add("climbing");
		leiHobbies.add("animals");
		List<Map.Entry<String, String>> leiEducation = new ArrayList<Map.Entry<String, String>>();
		leiEducation.add(new AbstractMap.SimpleEntry<String, String>("undergraduate", "KAIST"));
		BlueDevil lei = new BlueDevil("Lei", "Chen", "female", new ArrayList<Map.Entry<String, Integer>>(), 
			leiHobbies, "China", leiEducation, "master ECE candidate", "MEng", "lc294");
		BlueDevil.add(lei);
		
		// zhongyu
		List<String> zhongyuHobbies = new ArrayList<String>();
		zhongyuHobbies.add("basketball");
		zhongyuHobbies.add("NBA");
		List<Map.Entry<String, String>> zhongyuEducation = new ArrayList<Map.Entry<String, String>>();
		zhongyuEducation.add(new AbstractMap.SimpleEntry<String, String>("undergraduate", "SouthEast"));
		BlueDevil zhongyu = new BlueDevil("Zhongyu", "Li", "male", new ArrayList<Map.Entry<String, Integer>>(), 
			zhongyuHobbies, "China", zhongyuEducation, "master ECE candidate", "ECE", "sy167");
		BlueDevil.add(zhongyu);
		
		// shalin
		List<String> shalinHobbies = new ArrayList<String>();
		shalinHobbies.add("bodybuilding");
		shalinHobbies.add("dancing");
		List<Map.Entry<String, String>> shalinEducation = new ArrayList<Map.Entry<String, String>>();
		shalinEducation.add(new AbstractMap.SimpleEntry<String, String>("undergraduate", "SouthEast"));
		BlueDevil shalin = new BlueDevil("Shalin", "Shah", "male", new ArrayList<Map.Entry<String, Integer>>(), 
			shalinHobbies, "India", shalinEducation, "PHD ECE candidate", "ECE", "sns37");
		BlueDevil.add(shalin);
		
		// you
		List<String> youHobbies = new ArrayList<String>();
		youHobbies.add("travaling");
		youHobbies.add("music");
		youHobbies.add("history");
		List<Map.Entry<String, String>> youEducation = new ArrayList<Map.Entry<String, String>>();
		youEducation.add(new AbstractMap.SimpleEntry<String, String>("undergraduate", "U.K."));
		BlueDevil you = new BlueDevil("You", "Lyu", "female", new ArrayList<Map.Entry<String, Integer>>(), 
			youHobbies, "India", youEducation, "master ECE candidate", "ECE", "yl345");
		BlueDevil.add(you);
		// Adel
		List<String> adelHobbies = new ArrayList<String>();
		adelHobbies.add("tennis");
		adelHobbies.add("biking");
		adelHobbies.add("gardening");
		adelHobbies.add("cooking");
		List<Map.Entry<String, String>> adelEducation = new ArrayList<Map.Entry<String, String>>();
		adelEducation.add(new AbstractMap.SimpleEntry<String, String>("graduate", "NC State University"));
		List<Map.Entry<String, Integer>> adelPreExp = new ArrayList<Map.Entry<String, Integer>>();
		adelPreExp.add(new AbstractMap.SimpleEntry<String, Integer>("IBM", 30));
		BlueDevil adel = new BlueDevil("Adel", "Fahmy", "male", adelPreExp, 
			adelHobbies, "Egypt", adelEducation, "adjunct assistant professor", "ECE", "af123");
		BlueDevil.add(adel);
		// Ric
		List<String> ricHobbies = new ArrayList<String>();
		ricHobbies.add("golf");
		ricHobbies.add("sand volleyball");
		ricHobbies.add("swimming");
		ricHobbies.add("biking");
		List<Map.Entry<String, String>> ricEducation = new ArrayList<Map.Entry<String, String>>();
		ricEducation.add(new AbstractMap.SimpleEntry<String, String>("undergraduate", "Trinity University, San Antonio, TX"));
		List<Map.Entry<String, Integer>> ricPreExp = new ArrayList<Map.Entry<String, Integer>>();
		ricPreExp.add(new AbstractMap.SimpleEntry<String, Integer>("IBM", 32));
		BlueDevil ric = new BlueDevil("Ric", "Telford", "male", ricPreExp, 
			ricHobbies, "United States", ricEducation, "Executive-in-Residence and adjunct assistant professor", "ECE", "rt123");
		BlueDevil.add(ric);
		
		// liming
		List<String> limingHobbies = new ArrayList<String>();
		limingHobbies.add("hiking");
		List<Map.Entry<String, String>> limingEducation = new ArrayList<Map.Entry<String, String>>();
		limingEducation.add(new AbstractMap.SimpleEntry<String, String>("undergraduate", "Tongji University"));
		BlueDevil liming = new BlueDevil("Liming", "Jin", "male", new ArrayList<Map.Entry<String, Integer>>(), 
			limingHobbies, "China", limingEducation, "master ECE candidate", "ECE", "lj142");
		BlueDevil.add(liming);
		game();
	}
	// let the user to play a game to find, add or remove a person to the current group
	public static void game() {
		System.out.println("Search a person, add a person, or remove a person");
		System.out.println("Please use keywords: \"search\", \"add\", or \"remove\"");
		Scanner scanner = new Scanner(System.in);
		String choice = "";
		while (!choice.equals("search") && !choice.equals("add") 
				&& !choice.equals("remove")) {
			choice = scanner.nextLine();
			if (choice.equals("search")) {
				BlueDevil.findPerson();
				game();
			} else if (choice.equals("add")) {
				BlueDevil.addPerson();
				game();
			} else if (choice.equals("remove")) {
				BlueDevil.removePerson();
				game();
			} else {
				System.out.println("Please choose a mode: \"search\", \"add\", or \"remove\"");
			}
		}
	}
}
